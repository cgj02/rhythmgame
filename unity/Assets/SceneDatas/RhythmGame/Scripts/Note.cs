﻿using UnityEngine;
using System.Collections;

namespace Rhythm
{
    public class Note : MonoBehaviour
    {
        public NoteType type;

        // タップするタイミング
        public float hitTiming;

        public Vector3 startPosition;

        public Vector3 endPosition;

        public enum NoteType
        {
            None = 0, // ノーツ無し
            SingleTap, // 通常ノーツ
        }

    }
}