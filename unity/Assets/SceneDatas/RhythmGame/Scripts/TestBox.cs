﻿using UnityEngine;
using System.Collections;

public class TestBox : MonoBehaviour
{

    void Start()
    {
        this.GetComponent<AudioSource>().Play();
        Debug.Log("Start");
    }

    void FixedUpdate()
    {
        //if (Music.IsJustChangedSection())
        //if (Music.IsJustChangedBeat())
        //if (Music.IsJustChangedBar())
        if (Music.IsJustChangedBeat())
        {
            //Debug.Log("Changed");
            this.transform.position += new Vector3(0f, 1f, 0f);
            Debug.Log(" " + Music.MusicalTime  + " , " + Music.MusicalTime % 16f );
        }

        if (Input.GetMouseButtonDown(0))
        {
            OnClick();
        }
    }

    // マウスクリック時に呼ばれる
    void OnClick()
    {
        Debug.Log(" " + Music.MusicalTime % 8f);
    }
}
