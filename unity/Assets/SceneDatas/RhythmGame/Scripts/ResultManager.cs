﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour {

	[SerializeField]
	private AudioSource resultVoiceSource;

	private int score;

	public Sprite[] charas = new Sprite[4];
	public Sprite[] rairties = new Sprite[4];
	public AudioSource[] charaVoice = new AudioSource[4];
	public AudioSource[] resultVoice = new AudioSource[4];

	private int MaxScore;

	private int resultLevel;

	// Use this for initialization
	void Start () {

		this.MaxScore = 67;

		StartCoroutine ("AudioPlay");

		this.score = Rhythm.RhythmManager.resultScore;


		Sprite character = transform.Find ("CharacterImage").gameObject.GetComponent<Image> ().sprite;
		transform.Find ("PointBoard").GetComponent<Score> ().SetScore (this.score);

		if (this.score >= MaxScore * 0.8){
			transform.Find ("CharacterImage").gameObject.GetComponent<Image> ().sprite = charas [0];		
			transform.Find ("RarityImage").gameObject.GetComponent<Image> ().sprite =  rairties[0];		
			this.resultLevel = 0;
		} else if (this.score < MaxScore * 0.8 && this.score >= MaxScore * 0.7) {
			transform.Find ("CharacterImage").gameObject.GetComponent<Image> ().sprite = charas [1];
			transform.Find ("RarityImage").gameObject.GetComponent<Image> ().sprite =  rairties[1];
			this.resultLevel = 1;
		} else if (this.score < MaxScore * 0.7 && this.score >= MaxScore * 0.5) {
			transform.Find ("CharacterImage").gameObject.GetComponent<Image> ().sprite = charas [2];
			transform.Find ("RarityImage").gameObject.GetComponent<Image> ().sprite =  rairties[2];
			this.resultLevel = 2;
		} else {
			transform.Find ("CharacterImage").gameObject.GetComponent<Image> ().sprite = charas [3];
			transform.Find ("RarityImage").gameObject.GetComponent<Image> ().sprite =  rairties[3];
			this.resultLevel = 3;
		}
	}

	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeTitleScene()
	{
		SceneManager.LoadScene (SceneNames.TitleSceneName);
	}

	IEnumerator AudioPlay()
	{
		this.resultVoiceSource.Play ();

		yield return new WaitForSeconds (5);

		this.charaVoice [resultLevel].Play ();

		yield return new WaitForSeconds (3);

		this.charaVoice [resultLevel].Play ();

	}

}


