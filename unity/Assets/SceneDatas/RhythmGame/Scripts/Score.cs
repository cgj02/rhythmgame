﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public Sprite[] Numbers = new Sprite[10];

	private int score;

	void Start () 
	{
		this.score = 0;
	}

	void Update()
	{
		int score = this.score;

        {
			this.transform.FindChild("Num1").gameObject.SetActive (true);
			this.ChangeNumSprite (this.transform.FindChild ("Num1").gameObject, score % 10);
			score /= 10;
		}

        {
            this.transform.FindChild("Num10").gameObject.SetActive (true);
			this.ChangeNumSprite (this.transform.FindChild ("Num10").gameObject, score % 10);
			score /= 10;
		}

        {
			this.transform.FindChild("Num100").gameObject.SetActive (true);
			this.ChangeNumSprite (this.transform.FindChild ("Num100").gameObject, score % 10);
			score /= 10;
		}

	}
	
	public void SetScore(int score)
	{
		this.score = score;
	}

	public int GetScore()
	{
		return this.score;
	}

	void ChangeNumSprite(GameObject Num, int num)
	{
		Num.GetComponent<Image>().sprite = Numbers [num];
	}
}
