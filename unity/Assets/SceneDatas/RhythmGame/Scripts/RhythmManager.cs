﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

namespace Rhythm
{
    public class RhythmManager : MonoBehaviour
    {
        float sceneBeginTime = 0f;

        private const string TapEffectPrefabPath = "Prefab/TapEffect";

        private const int ScoreUnit = 2;

        // レーンの本数
        private const int LaneNum = 2;

        // 楽曲の再生が開始するまでの時間
        private float MusicWaitTime = 4f;

        // ノートを表示させるまでの時間
        private const float NoteBeginTime = 2.5f;

        // ノートがタップできるようになるまでの時間
        private const float BeforeTime = 0.6f;

        // ノートがタップ位置を通過してから消えるまでの時間
        private const float EndTime = -0.5f;


        private float musicDelayTime = 0.07f;

        // 楽曲を再生しているか
        private bool isPlayingMusic = false;

        // ノートを出し始める時間
        private int noteStartTiming = 0;

        public PlayerPosition playerPosition = PlayerPosition.Left;

        // ゲームのスコア
        public static int resultScore = 0;

        // プレイヤーの位置を示す長方形(左にいる場合に表示)
        public Image playerPositionRectLeft;
        // プレイヤーの位置を示す長方形(右にいる場合に表示)
        public Image playerPositionRectRight;

        public enum PlayerPosition
        {
            Left,
            Right
        }

        public enum NoteJudge
        {
            Good = 0,
            Cool,
            Perfect,
            Miss
        }

        public Sprite[] judgeImage;

        // スコア
        [SerializeField]
        private Score gamePoint;

        // コンボ数
        [SerializeField]
        private Score gameCombo;

        // 譜面データ
        [SerializeField]
        private ScoreMaster scoreMaster;

        [SerializeField]
        private float bpm = 140f;
        [SerializeField]
        private Note notePrefab;
        [SerializeField]
        private AudioSource bgmAudioSource;
        [SerializeField]
        private AudioSource tapAudioSource;
        [SerializeField]
        private Transform[] startPosition;
        [SerializeField]
        private Transform[] endPosition;
        [SerializeField]
        private List<Note>[] laneNotes = new List<Note>[LaneNum];

        [SerializeField]
        private GameObject nodeJudgePrefab;

        private GameObject tapEffectPrefab;
        private const float EffectTime = 2f;

        private List<Note> notes = new List<Note>();

        public int nextNote = 0;

        // すべてのノーツを出し終えた
        bool IsNotesEnd { get { return this.nextNote >= this.NoteNum; } }

        // 楽曲終了済み
        bool IsMusicEnd { get { return this.isMusicEnd; } }

        int NoteNum { get { return this.scoreMaster.sheets[0].list.Count; } }

        IEnumerator Start()
        {
            // シーン開始時の時刻を覚えておく
            this.sceneBeginTime = Time.time;

            // プレイヤーの位置をランダムに設定
            this.playerPosition = (Random.value < 0.5f) ? PlayerPosition.Left : PlayerPosition.Right;

            this.tapEffectPrefab = Resources.Load<GameObject>(TapEffectPrefabPath);

            for (int i = 0; i < this.laneNotes.Length; i++)
            {
                this.laneNotes[i] = new List<Note>();
            }

            yield return new WaitForSeconds(MusicWaitTime);

            this.MusicStart();
        }
        void UpdatePlayerPositionRect()
        {
            this.playerPositionRectLeft.enabled = false;
            this.playerPositionRectRight.enabled = false;

            switch (this.playerPosition)
            {
                case PlayerPosition.Left:
                    this.playerPositionRectLeft.enabled = true;
                    break;
                case PlayerPosition.Right:
                    this.playerPositionRectRight.enabled = true;
                    break;
            }
        }
        // 再生開始
        private void MusicStart()
        {
            this.isPlayingMusic = true;

            // 楽曲再生開始
            this.bgmAudioSource.Play();
        }

        void Update()
        {
            // プレイヤーの位置を示す長方形の更新
            this.UpdatePlayerPositionRect();

            // 楽曲終了判定
            if (isMusicEnd == false && this.GetPlayTime() > musicEndTime)
            {
                Debug.LogWarning("Music End");
                this.StartCoroutine(this.MusicQuit());
            }

            // ノーツの更新処理
            this.UpdateNotes();

            // タップ処理
            if (Input.GetMouseButtonDown(0))
            {
                this.OnTap();
            }
        }

        // ノーツの更新処理
        void UpdateNotes()
        {
            // ノートの生成処理
            this.CreateNotes();

            // ノート位置の更新処理
            this.UpdateNotesPosition();

            // ノートの終了判定
            this.CheckNoteEnd();
        }

        // ノーツの生成処理
        void CreateNotes()
        {
            if (this.IsMusicEnd) { return; }
            if (this.IsNotesEnd) { return; }

            float hitTiming = this.GetNextNoteTiming();
            float timeBeforeHit = hitTiming - this.GetPlayTime();

            if (timeBeforeHit <= NoteBeginTime)
            {
                this.CreateLaneNote(0);
                this.CreateLaneNote(1);
                nextNote++;
            }

        }

        bool isMusicEnd = false;
        float musicFadeOutWaitTime = 3.5f + BeforeTime;
        float musicFadeOutTime = 1.8f; // フェードアウトにかかる時間
        float musicMuteTime = 1f;
        float musicEndTime = 87f; // 楽曲終了時間
        IEnumerator MusicQuit()
        {
            this.isMusicEnd = true;
            yield return new WaitForSeconds(musicFadeOutWaitTime);

            float firstVolume = this.bgmAudioSource.volume;

            float t = 0f;
            while (t < musicFadeOutTime)
            {
                this.bgmAudioSource.volume = Mathf.Lerp(firstVolume, 0f, t / musicFadeOutTime);
                yield return null;
                t += Time.deltaTime;
            }
            yield return new WaitForSeconds(musicMuteTime);

            RhythmManager.resultScore = this.gamePoint.GetScore();
            SceneManager.LoadScene(SceneNames.ResultSceneName);
        }

        // レーンに対応するノーツの生成処理
        void CreateLaneNote(int lane)
        {
            if (this.IsNotesEnd) { return; }

            var type = this.GetNextNoteType(lane);

            if (type == Note.NoteType.None)
            {
                return;
            }

            float hitTiming = this.GetNextNoteTiming();
            switch (type)
            {
                case Note.NoteType.SingleTap:
                    {
                        var note = Instantiate(this.notePrefab);
                        note.transform.SetParent(this.transform);
                        note.hitTiming = hitTiming;
                        note.startPosition = this.startPosition[lane].position;
                        note.endPosition = this.endPosition[lane].position;

                        this.notes.Add(note);
                        this.laneNotes[lane].Add(note);

                        note.type = type;
                    }
                    break;
                default:
                    {
                    }
                    break;
            }


        }

        // ノーツの終了判定
        void CheckNoteEnd()
        {
            foreach (var note in this.notes)
            {
                float noteHitTiming = note.hitTiming;
                float noteTimeBeforeHit = noteHitTiming - this.GetPlayTime();

                // ノートタップ失敗
                if (noteTimeBeforeHit < EndTime)
                {
                    FailNote(note);
                }
            }

            this.notes.RemoveAll(x => x == null);
            foreach (var laneNote in this.laneNotes)
            {
                laneNote.RemoveAll(x => x == null);
            }
        }

        // ノーツの位置の更新処理
        private void UpdateNotesPosition()
        {
            foreach (var note in this.notes)
            {
                if (note == null) { continue; }

                float noteHitTiming = note.hitTiming;
                float noteTimeBeforeHit = noteHitTiming - this.GetPlayTime();

                float t = noteTimeBeforeHit / NoteBeginTime;
                note.transform.position = note.startPosition * t + note.endPosition * (1 - t);
            }
        }

        // ノーツの破棄
        void DestroyNote(Note note)
        {
            if (note == null) { return; }

            Destroy(note.gameObject);

            //// コルーチン　実行
            //StartCoroutine(Hoge());
        }

        //IEnumerator Hoge()
        //{
        //    f();

        //    // １秒まつ
        //    yield return new WaitForSeconds(1f);

        //    g();
        //}

        //　タップしたら呼ばれる
        void OnTap()
        {
            if (this.notes.Count == 0) { return; }

            List<Note> laneNote = null;
            switch (this.playerPosition)
            {
                case PlayerPosition.Left:
                    laneNote = this.laneNotes[0];
                    break;
                case PlayerPosition.Right:
                    laneNote = this.laneNotes[1];
                    break;
            }

            if (laneNote.Count == 0)
            {
                return;
            }
            var nextNote = laneNote[0];

            float hitTiming = nextNote.hitTiming;
            float timeBeforeHit = hitTiming - this.GetPlayTime();

            if (timeBeforeHit < BeforeTime)
            {

                TapNote(nextNote);
            }

        }

        private NoteJudge GetNoteJudge(Note note)
        {
            float hitTiming = note.hitTiming;
            float timeBeforeHit = hitTiming - this.GetPlayTime();

            float absTime = Mathf.Abs(timeBeforeHit);

            NoteJudge judge;
            if (absTime < 0.2f)
            {
                judge = NoteJudge.Perfect;
            }
            else
            if (absTime < 0.4f)
            {
                judge = NoteJudge.Cool;
            }
            else
            {
                judge = NoteJudge.Good;
            }

            return judge;
        }

        IEnumerator MoveJudgeSprite(GameObject go)
        {
            float t = 0f;

            Vector3 from = go.transform.position;
            Vector3 to = go.transform.position + new Vector3(0f, 14f, 0f);
            while (t < 1f)
            {
                t += Time.deltaTime;
                go.transform.position = Vector3.Lerp(from, to, t);
                yield return null;
            }

            Destroy(go);
        }

        // ノーツをタップする
        private void TapNote(Note note)
        {
            CreateJudgeSprite(note);

            // タップ音　再生
            this.tapAudioSource.Play();

            this.PlayTapEffect(note);

            // コンボ数更新
            this.gameCombo.SetScore(this.gameCombo.GetScore() + 1);

            // スコア更新
            this.gamePoint.SetScore(this.gamePoint.GetScore() + 1);

            switch (note.type)
            {
                case Note.NoteType.SingleTap:
                    this.DestroyNote(note);
                    break;
            }

        }

        private void CreateJudgeSprite(Note note)
        {
            if (note == null) { return; }
            var go = Instantiate(this.nodeJudgePrefab);

            var judge = GetNoteJudge(note);
            go.GetComponent<Image>().sprite = this.judgeImage[(int)judge];
            go.transform.SetParent(this.transform);
            go.transform.position = note.transform.position;

            StartCoroutine(this.MoveJudgeSprite(go));
        }

        private void CreateFailSprite(Note note)
        {
            if (note == null) { return; }
            var go = Instantiate(this.nodeJudgePrefab);

            NoteJudge judge = NoteJudge.Miss;
            go.GetComponent<Image>().sprite = this.judgeImage[(int)judge];
            go.transform.SetParent(this.transform);
            go.transform.position = note.transform.position;

            StartCoroutine(this.MoveJudgeSprite(go));
        }

        // タップエフェクトを出す
        private void PlayTapEffect(Note note)
        {
            GameObject go = Instantiate(this.tapEffectPrefab);
            go.transform.SetParent(this.transform);
            go.transform.position = note.transform.position;
            Destroy(go, EffectTime);
        }

        // ノーツタップ失敗
        private void FailNote(Note note)
        {
            this.CreateFailSprite(note);

            // コンボ数更新
            this.gameCombo.SetScore(0);

            this.DestroyNote(note);
        }


        // 現在の再生時間取得
        float GetPlayTime()
        {
            return Time.time - this.MusicWaitTime - musicDelayTime - this.sceneBeginTime;
        }


        // 次に出てくるノーツのタップ時間を取得
        float GetNextNoteTiming()
        {
            return this.nextNote * 60f / bpm / ScoreUnit;
        }

        // 次に出てくるノーツの種類を取得
        Note.NoteType GetNextNoteType(int lane)
        {
            switch (lane)
            {
                case 0:
                    return (Note.NoteType)this.scoreMaster.sheets[0].list[nextNote].notes1;
                case 1:
                    return (Note.NoteType)this.scoreMaster.sheets[0].list[nextNote].notes2;
                default:
                    throw new System.NotImplementedException();
            }
        }

        public int cnt = 0;
    }
}
