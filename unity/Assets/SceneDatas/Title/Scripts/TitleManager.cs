﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Title
{
    public class TitleManager : MonoBehaviour
    {
       public void GameStart()
		{
			SceneManager.LoadScene (SceneNames.RhythmSceneName);
		}
    }
}
