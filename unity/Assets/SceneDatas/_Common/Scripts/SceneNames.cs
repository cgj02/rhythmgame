﻿
public class SceneNames
{
	public readonly static string TitleSceneName  = "Title";
    public readonly static string RhythmSceneName = "RhythmGame";
	public readonly static string ResultSceneName = "ResultScene";
}
